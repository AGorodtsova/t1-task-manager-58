package ru.t1.gorodtsova.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.IListener;
import ru.t1.gorodtsova.tm.api.service.IServiceLocator;
import ru.t1.gorodtsova.tm.enumerated.Role;

@Setter
@Getter
@Component
public abstract class AbstractListener implements IListener {

    @Nullable
    @Autowired
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
