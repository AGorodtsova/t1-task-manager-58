package ru.t1.gorodtsova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.listener.AbstractListener;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
