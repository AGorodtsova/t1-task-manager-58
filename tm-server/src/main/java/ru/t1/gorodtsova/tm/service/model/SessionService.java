package ru.t1.gorodtsova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.gorodtsova.tm.api.repository.model.ISessionRepository;
import ru.t1.gorodtsova.tm.api.service.model.ISessionService;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.model.Session;

import javax.persistence.EntityManager;

@Service
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository();
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

}
