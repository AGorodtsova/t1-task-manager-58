package ru.t1.gorodtsova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.gorodtsova.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.gorodtsova.tm.api.service.dto.ISessionDtoService;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository() {
        return context.getBean(ISessionDtoRepository.class);
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionDtoRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

}
